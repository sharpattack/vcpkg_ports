set(VCPKG_POLICY_CMAKE_HELPER_PORT enabled)

set(VERSION "0.11.0-1")
set(USER_AGENT "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0")

set(FOLDER_NAME "xpack-openocd-${VERSION}")
set(ARCHIVE_NAME "")
set(DOWNLOAD_URL "")
set(HASH "")

if(VCPKG_CMAKE_SYSTEM_NAME STREQUAL "Linux")
    if(VCPKG_TARGET_ARCHITECTURE STREQUAL "x64")
        set(ARCHIVE_NAME "xpack-openocd-${VERSION}-linux-x64.tar.gz")
        set(HASH "5972fe70a274f054503dd519b68d3909b83f017b5b8dd2b59e84b3b72c9bc3e1")
    elseif(VCPKG_TARGET_ARCHITECTURE MATCHES "arm|aarch64")
        set(ARCHIVE_NAME "xpack-openocd-${VERSION}-linux-arm.tar.gz")
        set(HASH "24c5de0839b8c5cb3476d6fb7b9f528daba14b434a00d60ef71d4e4da3131262")
    endif()
elseif(VCPKG_CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    if(VCPKG_TARGET_ARCHITECTURE STREQUAL "x64")
        set(ARCHIVE_NAME "xpack-openocd-${VERSION}-darwin-x64.tar.gz")
        set(HASH "3e3719fd059d87f3433f1f6d8e37b8582e87ae6a168287eb32a85dbc0f2e1708")
    endif()
elseif(VCPKG_CMAKE_SYSTEM_NAME STREQUAL "WindowsStore")
    set(ARCHIVE_NAME "xpack-openocd-${version}-win32-ia32.zip")
    set(HASH "b556754ee621962d41f89b229115ecf5e067b6ae76e91e210d7f53d657769296")
endif()

if(ARCHIVE_NAME STREQUAL "")
    vcpkg_fail_port_install(MESSAGE "Your system ${VCPKG_CMAKE_SYSTEM_NAME}-${VCPKG_TARGET_ARCHITECTURE} is not supported")
endif()

set(DOWNLOAD_URL "https://github.com/xpack-dev-tools/openocd-xpack/releases/download/v0.11.0-1/${ARCHIVE_NAME}")

file(DOWNLOAD ${DOWNLOAD_URL}
    ${ARCHIVE_NAME}
    EXPECTED_HASH SHA256=${HASH}
    HTTPHEADER "User-Agent: ${USER_AGENT}"
)

file(ARCHIVE_EXTRACT INPUT ${ARCHIVE_NAME} DESTINATION "${CURRENT_PACKAGES_DIR}/tools/")

file(INSTALL "${CMAKE_CURRENT_LIST_DIR}/copyright" DESTINATION "${CURRENT_PACKAGES_DIR}/share/${PORT}")
configure_file("${CMAKE_CURRENT_LIST_DIR}/vcpkg-port-config.cmake" "${CURRENT_PACKAGES_DIR}/share/${PORT}/vcpkg-port-config.cmake" @ONLY)
vcpkg_add_to_path(PREPEND "${CURRENT_HOST_INSTALLED_DIR}/tools/openocd/bin")
