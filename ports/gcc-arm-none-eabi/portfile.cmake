set(VCPKG_POLICY_CMAKE_HELPER_PORT enabled)

set(VERSION "10.3-2021.10")
set(USER_AGENT "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/114.0")

set(FOLDER_NAME "gcc-arm-none-eabi-${VERSION}")
set(ARCHIVE_NAME "")
set(DOWNLOAD_URL "")
set(HASH "")

if(VCPKG_CMAKE_SYSTEM_NAME STREQUAL "Linux")
    if(VCPKG_TARGET_ARCHITECTURE STREQUAL "x64")
        set(ARCHIVE_NAME "gcc-arm-none-eabi-${VERSION}-x86_64-linux.tar.bz2")
        set(DOWNLOAD_URL "https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-x86_64-linux.tar.bz2?rev=78196d3461ba4c9089a67b5f33edf82a&hash=D484B37FF37D6FC3597EBE2877FB666A41D5253B")
        set(HASH "2383e4eb4ea23f248d33adc70dc3227e")
    elseif(VCPKG_TARGET_ARCHITECTURE MATCHES "arm|aarch64")
        set(ARCHIVE_NAME "gcc-arm-none-eabi-${VERSION}-aarch64-linux.tar.bz2")
        set(DOWNLOAD_URL "https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-aarch64-linux.tar.bz2?rev=b748c39178c043b4915b04645d7774d8&hash=572217C8AFE83F1010753EA3E3A7EC2307DADD58")
        set(HASH "3fe3d8bb693bd0a6e4615b6569443d0d")
    endif()
elseif(VCPKG_CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    if(VCPKG_TARGET_ARCHITECTURE STREQUAL "x64")
        set(ARCHIVE_NAME "gcc-arm-none-eabi-${VERSION}-mac.tar.bz2")
        set(DOWNLOAD_URL "https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-mac.tar.bz2?rev=58ed196feb7b4ada8288ea521fa87ad5&hash=62C9BE56E5F15D7C2D98F48BFCF2E839D7933597")
        set(HASH "7f2a7b7b23797302a9d6182c6e482449")
    endif()
elseif(VCPKG_CMAKE_SYSTEM_NAME STREQUAL "WindowsStore")
    set(ARCHIVE_NAME "gcc-arm-none-eabi-${version}-win32.exe")
    set(DOWNLOAD_URL "https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-win32.exe?rev=29bb46cfa0434fbda93abb33c1d480e6&hash=3C58D05EA5D32EF127B9E4D13B3244D26188713C")
    set(HASH "8d0f75f33f9e3d5f9600197626297212")
endif()

if(ARCHIVE_NAME STREQUAL "")
    vcpkg_fail_port_install(MESSAGE "Your system ${VCPKG_CMAKE_SYSTEM_NAME}-${VCPKG_TARGET_ARCHITECTURE} is not supported")
endif()

file(DOWNLOAD ${DOWNLOAD_URL}
    ${ARCHIVE_NAME}
    EXPECTED_HASH MD5=${HASH}
    HTTPHEADER "User-Agent: ${USER_AGENT}"
)

file(ARCHIVE_EXTRACT INPUT ${ARCHIVE_NAME} DESTINATION "${CURRENT_PACKAGES_DIR}/tools/")

file(INSTALL "${CMAKE_CURRENT_LIST_DIR}/copyright" DESTINATION "${CURRENT_PACKAGES_DIR}/share/${PORT}")

# Remove empty folders that vcpkg complains about
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/tools/gcc-arm-none-eabi-${VERSION}/arm-none-eabi/include/bits" "${CURRENT_PACKAGES_DIR}/tools/gcc-arm-none-eabi-${VERSION}/arm-none-eabi/include/rpc")

configure_file("${CMAKE_CURRENT_LIST_DIR}/gcc-arm-none-eabi-config.cmake" "${CURRENT_PACKAGES_DIR}/share/${PORT}/gcc-arm-none-eabi-config.cmake" @ONLY)
configure_file("${CMAKE_CURRENT_LIST_DIR}/vcpkg-port-config.cmake" "${CURRENT_PACKAGES_DIR}/share/${PORT}/vcpkg-port-config.cmake" @ONLY)
