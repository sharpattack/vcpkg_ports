set(VCPKG_POLICY_CMAKE_HELPER_PORT enabled)
set(VERSION "v1.11.1")

set(PLATFORM "")
if(VCPKG_CMAKE_SYSTEM_NAME STREQUAL "Linux")
  set(PLATFORM "linux")
elseif(VCPKG_CMAKE_SYSTEM_NAME STREQUAL "Darwin")
  set(PLATFORM "mac")
elseif(VCPKG_CMAKE_SYSTEM_NAME STREQUAL "WindowsStore")
  set(PLATFORM "win")
endif()

if(PLATFORM STREQUAL "")
    vcpkg_fail_port_install(MESSAGE "Your system ${VCPKG_CMAKE_SYSTEM_NAME}-${VCPKG_TARGET_ARCHITECTURE} is not supported")
endif()

set(ARCHIVE_NAME "ninja-${PLATFORM}.zip")
set(DOWNLOAD_URL "https://github.com/ninja-build/ninja/releases/download/${VERSION}/${ARCHIVE_NAME}")
file(DOWNLOAD ${DOWNLOAD_URL} ${ARCHIVE_NAME})

file(ARCHIVE_EXTRACT INPUT ${ARCHIVE_NAME} DESTINATION "${CURRENT_PACKAGES_DIR}/tools/ninja/")
file(INSTALL "${CMAKE_CURRENT_LIST_DIR}/copyright" DESTINATION "${CURRENT_PACKAGES_DIR}/share/${PORT}")
configure_file("${CMAKE_CURRENT_LIST_DIR}/vcpkg-port-config.cmake" "${CURRENT_PACKAGES_DIR}/share/${PORT}/vcpkg-port-config.cmake" @ONLY)

vcpkg_add_to_path(PREPEND "${CURRENT_HOST_INSTALLED_DIR}/tools/ninja")
