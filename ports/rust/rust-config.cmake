set(RUSTUP_HOME "${CMAKE_CURRENT_LIST_DIR}/../../tools/rust/rustup")
set(CARGO_HOME "${CMAKE_CURRENT_LIST_DIR}/../../tools/rust/cargo")
set(RUST_BIN "${CMAKE_CURRENT_LIST_DIR}/../../tools/rust/cargo/bin")

set(ENV{RUSTUP_HOME} "${CMAKE_CURRENT_LIST_DIR}/../../tools/rust/rustup")
set(ENV{CARGO_HOME} "${CMAKE_CURRENT_LIST_DIR}/../../tools/rust/cargo")
