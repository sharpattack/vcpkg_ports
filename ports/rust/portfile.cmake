set(VCPKG_POLICY_CMAKE_HELPER_PORT enabled)
set(VERSION 1.70.0)

if(VCPKG_CMAKE_SYSTEM_NAME STREQUAL "WindowsStore")
    vcpkg_fail_port_install(MESSAGE "Your system ${VCPKG_CMAKE_SYSTEM_NAME}-${VCPKG_TARGET_ARCHITECTURE} is not supported")
endif()

set(ENV{RUSTUP_HOME} "${CURRENT_PACKAGES_DIR}/tools/${PORT}/rustup")
set(ENV{CARGO_HOME} "${CURRENT_PACKAGES_DIR}/tools/${PORT}/cargo")

execute_process(COMMAND sh "${CMAKE_CURRENT_LIST_DIR}/rustup-init.sh" -y --no-modify-path
    COMMAND_ERROR_IS_FATAL ANY
)

file(INSTALL "${CMAKE_CURRENT_LIST_DIR}/copyright" DESTINATION "${CURRENT_PACKAGES_DIR}/share/${PORT}")
file(INSTALL "${CMAKE_CURRENT_LIST_DIR}/vcpkg-port-config.cmake" DESTINATION "${CURRENT_PACKAGES_DIR}/share/${PORT}")
file(INSTALL "${CMAKE_CURRENT_LIST_DIR}/rust-config.cmake" DESTINATION "${CURRENT_PACKAGES_DIR}/share/${PORT}")

vcpkg_add_to_path(PREPEND "$ENV{CARGO_HOME}/bin")

# Install and set default rust version
execute_process(COMMAND "${CURRENT_PACKAGES_DIR}/tools/${PORT}/cargo/bin/rustup" default ${VERSION}
    COMMAND_ERROR_IS_FATAL ANY
)

# Install arm target
execute_process(COMMAND "${CURRENT_PACKAGES_DIR}/tools/${PORT}/cargo/bin/rustup" target add thumbv7em-none-eabihf
    COMMAND_ERROR_IS_FATAL ANY
)

# Install cbindgen
execute_process(COMMAND "${CURRENT_PACKAGES_DIR}/tools/${PORT}/cargo/bin/cargo" install cbindgen --force
    COMMAND_ERROR_IS_FATAL ANY
)

# Remove files to make vcpkg happy
file(REMOVE_RECURSE "${CURRENT_PACKAGES_DIR}/tools/${PORT}/rustup/downloads" "${CURRENT_PACKAGES_DIR}/tools/${PORT}/rustup/tmp" "${CURRENT_PACKAGES_DIR}/tools/${PORT}/cargo/env")